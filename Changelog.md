# Changes in srhinow/simple-map-bundle
## 1.0.11 (20.09.2023)
- Patchwork\Utf8::strtoupper() => mb_strtoupper()

## 1.0.10 (20.09.2023)
- fix leafletCustomScript: add NOT NULL and dca - 'default'

## 1.0.9 (20.09.2023)
- fix leafletCustomScript: remove NOT NULL

## 1.0.8 (19.09.2023)
- fix PHP8: confirm in dca-files

## 1.0.7 (05.09.2023)
- add leafletCustomScript

## 1.0.6 (31.08.2023)
- add popupOpen in map-pin-settings

## 1.0.5 (23.08.2023)
- add mapboxStyle to simple-Map settings
## 1.0.4 (18.08.2023)
- update OsmHelper.php: delete get-Parameter 'postalcode' from Query-URL

## 1.0.3 (18.08.2023)
- update OsmHelper: fix nominatim.openstreetmap.org - url

## 1.0.2 (19.12.2022)
- get only pins with lat&lon paramter for frontend-view

## 1.0.1 (25.11.2022)
- fix conflicts with 4.13

## 1.0.0 (18.08.2022)
- merge csv_import

## 0.2.5 (22.05.2020)

