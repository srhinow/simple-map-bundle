<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\SimpleMapBundle;


use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the Contao simple-map-bundle.
 */
class SrhinowSimpleMapBundle extends Bundle
{}
