<?php
/**
 * Created by fachstellen.de.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 12.08.22
 */

namespace Srhinow\SimpleMapBundle\Helper;

use Contao\DataContainer;
use Spreefels\ContaoCasemanager\Helper\CallHelper;
use Srhinow\SimpleMapBundle\Models\SimpleMapPinModel;

class MapPinHelper
{
    /**
     * Object instance (Singleton).
     *
     * @var MapPinHelper
     */
    protected static $objInstance;

    protected function __construct()
    {
    }

    /**
     * Instantiate a new CallHelper object (Factory).
     *
     * @return static The object instance
     */
    public static function getInstance()
    {
        if (null === static::$objInstance) {
            static::$objInstance = new static();
        }

        return static::$objInstance;
    }

    /**
     * set the geolocation if its empty.
     */
    public function setGeoLatLonFromDc(DataContainer $dc): void
    {
        $searchData = $this->getGeoParamsFromDc($dc);
        if (!is_array($searchData) || count($searchData) < 1) {
            return;
        }

        $this->setGeoLatLon((int) $dc->id, (array) $searchData);
    }

    public function setGeoLatLon(int $pinId, array $searchData)
    {
        $geo = new OsmHelper();
        $data = $geo->getGeoData($searchData);

        if (\is_array($data) && count($data) > 0) {
            $objMapPin = SimpleMapPinModel::findByPk($pinId);
            if (null === $objMapPin) {
                return;
            }

            $objMapPin->setNewGeo = '';
            $objMapPin->mapLat = (float) $data[0]['lat'];
            $objMapPin->mapLon = (float) $data[0]['lon'];
            $objMapPin->save();
        }
    }

    /**
     * bereitet das Übergabe-Array für die OSM-Abrage vor.
     *
     * @return array
     */
    public function getGeoParamsFromDc(DataContainer $dc)
    {
        if (!\is_object($dc)) {
            return [];
        }

        $addressStr = urlencode(
            $dc->activeRecord->street.' '.$dc->activeRecord->nr
            .', '.$dc->activeRecord->city
        );

        return [
            'postal' => $dc->activeRecord->postal,
            'lang' => $dc->activeRecord->language,
            'address' => $addressStr,
            'string' => $dc->activeRecord->postal.' '.$dc->activeRecord->city.' '
                .$dc->activeRecord->street.' '.$dc->activeRecord->nr,
        ];
    }

    /**
     * bereitet das Übergabe-Array für die OSM-Abrage vor.
     *
     * @return array
     */
    public function getGeoParamsFromModel(SimpleMapPinModel $objMapPin)
    {
        if (!\is_object($objMapPin)) {
            return [];
        }

        $addressStr = urlencode(
            $objMapPin->street.' '.$objMapPin->nr
            .', '.$objMapPin->city
        );

        return [
            'postal' => $objMapPin->postal,
            'lang' => $objMapPin->language,
            'address' => $addressStr,
            'string' => $objMapPin->postal.' '.$objMapPin->city.' '
                .$objMapPin->street.' '.$objMapPin->nr,
        ];
    }
    
}