<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\SimpleMapBundle\Helper;


use Symfony\Component\HttpClient\HttpClient;

class OsmHelper
{
    public $geoData;
    private $url = 'https://nominatim.openstreetmap.org/search?q=##ADDRESS##&format=json&countrycodes=##LANG##&addressdetails=1&limit=1';
    private $server = false;

    /**
     * OsmHelper constructor.
     *
     * @param bool $query
     */
    public function __construct($query = false)
    {
        $this->setServer();
        if ($query) {
            return $this->getGeoData((array) $query);
        }
    }

    public function setServer($s = false): void
    {
        if (!$this->server) {
            $this->server = 'http://'.$_SERVER['HTTP_HOST'];
        }
        if ($s) {
            $this->server = $s;
        }
    }

    /**
     * get geo-data for a location-string.
     *
     * @var array
     *
     * @return json-string
     */
    public function getGeoData($query)
    {
        $address = trim($query['address']);
        $lang = \strlen($query['lang']) > 0 ? $query['lang'] : 'de';
        $this->url = str_replace('##ADDRESS##', $address, $this->url);
        $this->url = str_replace('##LANG##', $lang, $this->url);

        $client = HttpClient::create();
        $response = $client->request(
            'GET',
            $this->url,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-type' => 'application/json',
                ],
            ]
        );

        return $response->toArray();
    }
}
