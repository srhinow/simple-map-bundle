<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\SimpleMapBundle\Elements;

use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\System;
use Srhinow\SimpleMapBundle\Models\SimpleMapModel;
use Srhinow\SimpleMapBundle\Models\SimpleMapPinModel;
use Srhinow\SimpleMapBundle\Service\ModeService;

/**
 * Class ContentSimpleMap.
 */
class ContentSimpleMap extends ContentElement
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'ce_sm_map-view';

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        /** @var ModeService $ModeService */
        $ModeService = System::getContainer()->get('srhinow.sumple_map_bundle.service.mode_service');

        if ($ModeService->isBackend())
        {
            /** @var \BackendTemplate|object $objTemplate */
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### '.mb_strtoupper($GLOBALS['TL_LANG']['FMD']['simple_map_view'][0]).' ###';
            $objTemplate->title = $this->headline;

            return $objTemplate->parse();
        }

        if (\strlen($this->customTpl) > 0) {
            $this->strTemplate = $this->customTpl;
        }

        return parent::generate();
    }

    /**
     * Generate the module.
     */
    protected function compile(): void
    {
        //hole Kartendaten
        $objSimpleMap = SimpleMapModel::findByPk($this->simpleMap);
        if (null === $objSimpleMap) {
            $this->Template->error = true;
            $this->Template->msg = 'Es ist keine Karte übergeben worden.';

            return;
        }

        //hole Pindaten
        $objMapPins = SimpleMapPinModel::findBy(['pid=?', 'published=?'], [$this->simpleMap, 1]);
        if (null === $objMapPins) {
            $this->Template->error = true;
            $this->Template->msg = 'Es sind keine Karte Markierungen für die Karte vorhanden.';

            return;
        }

        $marker = [];
        while ($objMapPins->next()) {
            $marker[] = $objMapPins->row();
        }

        $this->Template->map = $objSimpleMap->row();
        $this->Template->marker = $marker;
        $this->Template->mapId = 'map'.$objSimpleMap->id;

        $GLOBALS['TL_HEAD'][] = '<link rel="stylesheet" 
            href="'.$GLOBALS['BE_SIMPLE_MAP_BUNDLE']['PROPERTIES']['PUBLICSRC'].'/leaflet/leaflet.css" />';
        $GLOBALS['TL_HEAD'][] = '<script 
            src="'.$GLOBALS['BE_SIMPLE_MAP_BUNDLE']['PROPERTIES']['PUBLICSRC'].'/leaflet/leaflet.js"></script>';
    }
}
