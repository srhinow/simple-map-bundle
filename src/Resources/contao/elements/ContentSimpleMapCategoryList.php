<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\SimpleMapBundle\Elements;

use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\System;
use Srhinow\SimpleMapBundle\Models\SimpleMapCategoryModel;
use Srhinow\SimpleMapBundle\Models\SimpleMapModel;
use Srhinow\SimpleMapBundle\Models\SimpleMapPinModel;
use Srhinow\SimpleMapBundle\Service\ModeService;

/**
 * Class ContentSimpleMapCategoryList.
 */
class ContentSimpleMapCategoryList extends ContentElement
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'ce_sm_category-list';

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        /** @var ModeService $ModeService */
        $ModeService = System::getContainer()->get('srhinow.sumple_map_bundle.service.mode_service');

        if ($ModeService->isBackend()) {
            /** @var \BackendTemplate|object $objTemplate */
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard =
                '### '.mb_strtoupper($GLOBALS['TL_LANG']['CTE']['simple_map_category_list'][0]).' ###';
            $objTemplate->title = $this->headline;

            return $objTemplate->parse();
        }

        if (\strlen($this->simpleMapTemplate) > 0) {
            $this->strTemplate = $this->simpleMapTemplate;
        }

        return parent::generate();
    }

    /**
     * Generate the module.
     */
    protected function compile(): void
    {
        //hole Kartendaten
        $objSimpleMap = SimpleMapModel::findByPk($this->simpleMap);
        if (null === $objSimpleMap) {
            $this->Template->error = true;
            $this->Template->msg = 'Es ist keine Karte übergeben worden.';

            return;
        }

        $objMapPins = SimpleMapPinModel::findBy(['pid=?', 'published=?'], [$this->simpleMap, 1]);
        if (null === $objMapPins) {
            $this->Template->error = true;
            $this->Template->msg = 'Es sind keine Karte Markierungen für die Karte vorhanden.';

            return;
        }

        $marker = [];
        while ($objMapPins->next()) {
            $marker[$objMapPins->category][] = $objMapPins->row();
        }

        $this->Template->map = $objSimpleMap->row();
        $this->Template->categories = $this->getSortCategoryArray();
        $this->Template->marker = $marker;
    }

    /**
     * gibt ein sortiertes Array an Kategorien zurück.
     *
     * @return array
     */
    protected function getSortCategoryArray()
    {
        $arrReturn = [];
        $objCategories = SimpleMapCategoryModel::findAll();
        if (null === $objCategories) {
            return $arrReturn;
        }

        while ($objCategories->next()) {
            $arrReturn[$objCategories->sorting] = $objCategories->row();
        }

        ksort($arrReturn, SORT_NUMERIC);

        return $arrReturn;
    }
}
