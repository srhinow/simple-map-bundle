<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\SimpleMapBundle\Modules\Frontend;

/*
 * Run in a custom namespace, so the class can be replaced
 */

use Contao\System;
use Srhinow\SimpleMapBundle\Models\SimpleMapModel;
use Srhinow\SimpleMapBundle\Models\SimpleMapPinModel;
use Srhinow\SimpleMapBundle\Service\ModeService;

/**
 * Class ModuleSimpleRecipesList.
 *
 * Front end module "simple_map_view"
 */
class ModuleSimpleMapView extends ModuleSimpleMap
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'mod_simple_map';

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        /** @var ModeService $ModeService */
        $ModeService = System::getContainer()->get('srhinow.sumple_map_bundle.service.mode_service');

        if ($ModeService->isBackend()) {
            $objTemplate = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### '.mb_strtoupper($GLOBALS['TL_LANG']['CTE']['simple_map_view'][0]).' ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }

    /**
     * Generate the module.
     */
    protected function compile(): void
    {
        //hole Kartendaten
        $objSimpleMap = SimpleMapModel::findByPk($this->simpleMap);
        if (null === $objSimpleMap) {
            $this->Template->error = true;
            $this->Template->msg = 'Es ist keine Karte übergeben worden.';

            return;
        }

        //hole Pindaten
        $objMapPins = SimpleMapPinModel::findBy(
            ['pid=?', 'published=?','mapLat!=?','mapLon!=?'],
            [$this->simpleMap, 1,'','']
        );

        if (null === $objMapPins) {
            $this->Template->error = true;
            $this->Template->msg = 'Es sind keine Karte Markierungen für die Karte vorhanden.';

            return;
        }

        $marker = [];
        while ($objMapPins->next()) {
            $marker[] = $objMapPins->row();
        }

        $this->Template->map = $objSimpleMap->row();
        $this->Template->marker = $marker;
        $this->Template->mapId = 'map'.$objSimpleMap->id;

        $GLOBALS['TL_HEAD'][] = '<link rel="stylesheet" 
            href="'.$GLOBALS['BE_SIMPLE_MAP_BUNDLE']['PROPERTIES']['PUBLICSRC'].'/leaflet/leaflet.css" />';
        $GLOBALS['TL_HEAD'][] = '<script 
            src="'.$GLOBALS['BE_SIMPLE_MAP_BUNDLE']['PROPERTIES']['PUBLICSRC'].'/leaflet/leaflet.js"></script>';
    }
}
