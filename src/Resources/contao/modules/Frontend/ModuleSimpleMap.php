<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\SimpleMapBundle\Modules\Frontend;

/*
 * Run in a custom namespace, so the class can be replaced
 */
use Contao\Module;

/**
 * Class ModuleSimpleMap.
 */
abstract class ModuleSimpleMap extends Module
{
}
