<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_LANG']['tl_content']['simpleMap'] = [
    'Karte',
    'Wählen sie hier die Karte aus die dargestellt werden soll.',
];
$GLOBALS['TL_LANG']['tl_content']['simpleMapTemplate'] = [
    'Karten-Template',
    'Wählen sie für die Darstellung das passende Template aus.',
];

//# Legend
$GLOBALS['TL_LANG']['tl_content']['module_legend'] = 'Modul-Einstellungen';
