<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

/*
 * Global Operations
 */
$GLOBALS['TL_LANG']['tl_simple_map']['importFromCsv'][0] = 'CSV-Import';
$GLOBALS['TL_LANG']['tl_simple_map']['importFromCsv'][1] = 'Fälle aus einem CSV-Datei importieren.';

/*
 * Legend.
 */
$GLOBALS['TL_LANG']['tl_simple_map']['main_legend'] = 'Haupteinstellungen';
$GLOBALS['TL_LANG']['tl_simple_map']['address_legend'] = 'Adressdaten';
$GLOBALS['TL_LANG']['tl_simple_map']['map_legend'] = 'Karten-Einstellungen';
$GLOBALS['TL_LANG']['tl_simple_map']['mapbox_legend'] = 'Mapbox-Einstellungen';

/*
 * Fields
 */
$GLOBALS['TL_LANG']['tl_simple_map']['title'][0] = 'Titel';
$GLOBALS['TL_LANG']['tl_simple_map']['title'][1] = 'Titel der Karte';
$GLOBALS['TL_LANG']['tl_simple_map']['alias'][0] = 'Alias';
$GLOBALS['TL_LANG']['tl_simple_map']['alias'][1] = 'wird z.B. für die URL-Übergabe benötigt';
$GLOBALS['TL_LANG']['tl_simple_map']['do_mapbox'][0] = 'Mapbox verwenden';
$GLOBALS['TL_LANG']['tl_simple_map']['mapboxApiKey'][0] = 'Mapbox-API-Key';
$GLOBALS['TL_LANG']['tl_simple_map']['mapboxApiKey'][1] = 'Dies wird zur Darstellung mit dem Mapbox-Service benötigt';
$GLOBALS['TL_LANG']['tl_simple_map']['mapboxStyle'] = ['Mapbox-Style','Es können hier Classic Mapbox Styles https://docs.mapbox.com/api/maps/styles/#classic-mapbox-styles in der Form "mapbox/streets-v12"'];
$GLOBALS['TL_LANG']['tl_simple_map']['leafletCustomScript'] = ['weitere Leaflet-Einstellungen (optional)','Es können hier weitere map-Definitionen wie "mymap.scrollWheelZoom.disable();" eingefügt werden. (https://leafletjs.com/reference.html) Am besten, jeder Befehl in einer neuen Zeile und <strong>ohne umschließendes &lt;script&gt;-Tag</strong>.'];
$GLOBALS['TL_LANG']['tl_simple_map']['setNewGeo'] = [
    'Geo-Koordinaten setzen',
    'Geo-Koordinaten neu aus Adressdaten ermitteln.',
];
$GLOBALS['TL_LANG']['tl_simple_map']['setNewPin'] = [
    'Pin erstellen',
    'Einen neuen Pin aus diesen Adressdaten ermitteln. Falls 
    z.B. nur ein Pin dargestellt werden soll kürzt dies die Einrichtung etwas ab.',
];

$GLOBALS['TL_LANG']['tl_simple_map']['street'] = [
    'Strasse',
];

$GLOBALS['TL_LANG']['tl_simple_map']['nr'] = [
    'Hausnummer',
];

$GLOBALS['TL_LANG']['tl_simple_map']['postal'] = [
    'Postleitzahl',
];

$GLOBALS['TL_LANG']['tl_simple_map']['city'] = [
    'Stadt',
];

$GLOBALS['TL_LANG']['tl_simple_map']['language'] = [
    'Land (Sprache)',
];

$GLOBALS['TL_LANG']['tl_simple_map']['mapLat'][0] = 'Karten-Koordinate (latitude)';
$GLOBALS['TL_LANG']['tl_simple_map']['mapLat'][1] = 'Auf dieser Koordinate wird die Karte zentriert.';
$GLOBALS['TL_LANG']['tl_simple_map']['mapLon'][0] = 'Karten-Koordinate (longitude)';
$GLOBALS['TL_LANG']['tl_simple_map']['mapLon'][1] = 'Auf dieser Koordinate wird die Karte zentriert.';
$GLOBALS['TL_LANG']['tl_simple_map']['mapZoom'] = [
    'Karten-Zoom',
    'Bereich zwischen 1-10. Auf dieser Zahl wird die Kartenausschnitt herreingezoom.',
];

/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_simple_map']['new'][0] = 'Neue Karte';
$GLOBALS['TL_LANG']['tl_simple_map']['new'][1] = 'Eine neue Karte anlegen.';
$GLOBALS['TL_LANG']['tl_simple_map']['edit'][0] = 'Karte bearbeiten';
$GLOBALS['TL_LANG']['tl_simple_map']['edit'][1] = 'Karte ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_simple_map']['map_pin'][0] = 'Markierungen';
$GLOBALS['TL_LANG']['tl_simple_map']['map_pin'][1] = 'Markierungen zu der Karte bearbeiten.';
$GLOBALS['TL_LANG']['tl_simple_map']['copy'][0] = 'Karte duplizieren';
$GLOBALS['TL_LANG']['tl_simple_map']['copy'][1] = 'Karte ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_simple_map']['delete'][0] = 'Karte löschen';
$GLOBALS['TL_LANG']['tl_simple_map']['delete'][1] = 'Karte ID %s löschen.';
$GLOBALS['TL_LANG']['tl_simple_map']['show'][0] = 'Kartendetails anzeigen';
$GLOBALS['TL_LANG']['tl_simple_map']['show'][1] = 'Details für Karte ID %s anzeigen.';
