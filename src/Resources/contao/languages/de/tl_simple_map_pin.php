<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */
$GLOBALS['TL_LANG']['tl_simple_map_pin']['importFromCsv'] = [
    'CSV-Import',
    'Karten-Markierungen aus einer CSV-Datei importieren.',
];
/*
 * Fields.
 */
$GLOBALS['TL_LANG']['tl_simple_map_pin']['title'] = [
    'Bezeichnung der Markierung',
    'Wird in der Backend-Liste angezeigt.',
];

$GLOBALS['TL_LANG']['tl_simple_map_pin']['category'] = [
    'Kategorie',
    'Hier besteht die Möglichkeit eine Kategorie für die Frontend-Darstellung zuzuweisen.',
];

$GLOBALS['TL_LANG']['tl_simple_map_pin']['setNewGeo'] = [
    'Geo-Koordinaten setzen',
    'Geo-Koordinaten neu aus Adressdaten ermitteln.',
];

$GLOBALS['TL_LANG']['tl_simple_map_pin']['street'] = [
    'Strasse',
];

$GLOBALS['TL_LANG']['tl_simple_map_pin']['nr'] = [
    'Hausnummer',
];

$GLOBALS['TL_LANG']['tl_simple_map_pin']['postal'] = [
    'Postleitzahl',
];

$GLOBALS['TL_LANG']['tl_simple_map_pin']['city'] = [
    'Stadt',
];

$GLOBALS['TL_LANG']['tl_simple_map_pin']['language'] = [
    'Land (Sprache)',
];

$GLOBALS['TL_LANG']['tl_simple_map_pin']['published'] = [
    'Für Website-Besucher sichtbar',
    'Geben sie hier ob das Produkt auf der Website angezeit werden soll.',
];

$GLOBALS['TL_LANG']['tl_simple_map_pin']['mapLat'] = [
    'Karten-Koordinate (latitude)',
    'Auf dieser Koordinate wird die Karte zentriert.',
];

$GLOBALS['TL_LANG']['tl_simple_map_pin']['mapLon'] = [
    'Karten-Koordinate (longitude)',
    'Auf dieser Koordinate wird die Karte zentriert.',
];

$GLOBALS['TL_LANG']['tl_simple_map_pin']['mapZoom'] = [
    'Karten-Zoom',
    'Bereich zwischen 1-10. Auf dieser Zahl wird die Kartenausschnitt herreingezoom.',
];

$GLOBALS['TL_LANG']['tl_simple_map_pin']['popupText'] = [
    'Popup-Text',
    'Kurze Infos die in der Infoblase angezeigt wird wenn man den Pin anklickt.',
];
$GLOBALS['TL_LANG']['tl_simple_map_pin']['popupOpen'] = [
    'Popup anzeigen',
    'Beim laden der Karte das Popup bereits geöffnet anzeigen. (Macht meist nur Sinn, wenn es nur einen Pin auf der Karte gibt.)',
];

/* CSV-Import */
$GLOBALS['TL_LANG']['tl_simple_map_pin']['csv_import_test'] = 'Es würden %s von %s Einträgen der Datei %s importiert:';
$GLOBALS['TL_LANG']['tl_simple_map_pin']['csv_imported'] = 'Es wurden %s von %s aus der Datei %s importiert.';
$GLOBALS['TL_LANG']['tl_simple_map_pin']['csv_loop_imported'] = 'Es wurden %s von insgesamt %s aus der Datei %s importiert.';
$GLOBALS['TL_LANG']['tl_simple_map_pin']['importCsvFile'] = [
    'CSV-Datei',
    'z.B. tl_simple_map_pin.csv',
];
$GLOBALS['TL_LANG']['tl_simple_map_pin']['importMatchMapPinFields'] = [
    'CSV-Spalten-Zuweisung (Map-Pins)',
    'Geben Sie im linken Feld entweder den Spaltennamen aus der ersten Reihe oder die Nummer 1-... ein, im mittleren Feld in welcher DB-Spalte es importiert weden soll und das rechte Feld steht optional für Werte bereit, die automatisch zu jedem Eintrag mit angelegt werden sollen.',
];

$GLOBALS['TL_LANG']['tl_simple_map_pin']['tl_csv_fields'] = [
    'CSV-Spaltenname / Spaltenzahl',
];
$GLOBALS['TL_LANG']['tl_simple_map_pin']['custom_value'] = [
    'eigener Wert',
];

$GLOBALS['TL_LANG']['tl_simple_map_pin']['saveMatchFields'] = [
    'Zuweisung speichern',
    'Die letzten Feldzuweisung für den nächsten Import speichern.',
];

$GLOBALS['TL_LANG']['tl_simple_map_pin']['importOffset'] = [
    'Import-Offset',
    'Um anzugeben ab welchen Datensatz der Import beginnen soll.',
];
$GLOBALS['TL_LANG']['tl_simple_map_pin']['importLimit'] = [
    'Import-Limit',
    'Um große Importe in kleinere Hpchen zu zerteilen und mit einem Reload des Browserfensters zu trennen.',
];
$GLOBALS['TL_LANG']['tl_simple_map_pin']['runTest'] = [
    'nur Testlauf',
    'Es werden keine Datenbankeinträge angelegt und auch kein Workflow ausgeführt.',
];
$GLOBALS['TL_LANG']['tl_simple_map_pin']['importCSV'] = ['Import starten'];
/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_simple_map_pin']['new'][0] = 'Neue Markierung';
$GLOBALS['TL_LANG']['tl_simple_map_pin']['new'][1] = 'Eine neue Markierung anlegen.';
$GLOBALS['TL_LANG']['tl_simple_map_pin']['edit'][0] = 'Markierung bearbeiten';
$GLOBALS['TL_LANG']['tl_simple_map_pin']['edit'][1] = 'Markierung ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_simple_map_pin']['copy'][0] = 'Markierung duplizieren';
$GLOBALS['TL_LANG']['tl_simple_map_pin']['copy'][1] = 'Markierung ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_simple_map_pin']['delete'][0] = 'Markierung löschen';
$GLOBALS['TL_LANG']['tl_simple_map_pin']['delete'][1] = 'Markierung ID %s löschen.';
$GLOBALS['TL_LANG']['tl_simple_map_pin']['show'][0] = 'Markierungdetails anzeigen';
$GLOBALS['TL_LANG']['tl_simple_map_pin']['show'][1] = 'Details für Markierung ID %s anzeigen.';

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_simple_map_pin']['main_legend'] = 'Haupt-Einstellungen';
$GLOBALS['TL_LANG']['tl_simple_map_pin']['address_legend'] = 'Adressdaten';
$GLOBALS['TL_LANG']['tl_simple_map_pin']['map_legend'] = 'Markierungs-Einstellungen';
$GLOBALS['TL_LANG']['tl_simple_map_pin']['general_legend'] = 'weitere Einstellungen';
