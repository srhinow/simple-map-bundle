<?php

declare(strict_types=1);

use Contao\System;
use Symfony\Component\HttpFoundation\Request;

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['BE_SIMPLE_MAP_BUNDLE']['PROPERTIES']['PUBLICSRC'] = 'bundles/srhinowsimplemap';
$GLOBALS['BE_SIMPLE_MAP_BUNDLE']['csv_seperators'] = ['semicolon'=>';', 'comma'=>',',  'tabulator'=>'\t', 'linebreak'=>'\n'];
/*
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------
 */
if (System::getContainer()->get('contao.routing.scope_matcher')->isBackendRequest(System::getContainer()->get('request_stack')->getCurrentRequest() ?? Request::create(''))) {
    $GLOBALS['TL_CSS'][] = $GLOBALS['BE_SIMPLE_MAP_BUNDLE']['PROPERTIES']['PUBLICSRC'].'/css/be.css|static';
}

Contao\ArrayUtil::arrayInsert($GLOBALS['BE_MOD'], 1, ['simple_map_bundle' => []]);

$GLOBALS['BE_MOD']['simple_map_bundle']['simple_map'] = [
    'tables' => ['tl_simple_map', 'tl_simple_map_pin'],
    'icon' => $GLOBALS['BE_SIMPLE_MAP_BUNDLE']['PROPERTIES']['PUBLICSRC'].'/icons/pin_map.png',
    'importFromCsv'=> array('srhinow.simple_map_bundle.service.csv_import', 'importMapPins'),
];

$GLOBALS['BE_MOD']['simple_map_bundle']['simple_map_category'] = [
    'tables' => ['tl_simple_map_category'],
    'icon' => $GLOBALS['BE_SIMPLE_MAP_BUNDLE']['PROPERTIES']['PUBLICSRC'].'/icons/category.png',
];
/*
 * -------------------------------------------------------------------------
 * CONTENT ELEMENT
 * -------------------------------------------------------------------------
 */
Contao\ArrayUtil::arrayInsert($GLOBALS['TL_CTE'], 1, ['simple_map_bundle' => []]);
$GLOBALS['TL_CTE']['simple_map']['simple_map_view'] = 'Srhinow\SimpleMapBundle\Elements\ContentSimpleMap';
$GLOBALS['TL_CTE']['simple_map']['simple_map_category_list'] =
    'Srhinow\SimpleMapBundle\Elements\ContentSimpleMapCategoryList';

/*
 * -------------------------------------------------------------------------
 * Front END MODULES
 * -------------------------------------------------------------------------
 */
Contao\ArrayUtil::arrayInsert($GLOBALS['FE_MOD'], 2, [
    'simple_map_bundle' => [
        'simple_map_view' => 'Srhinow\SimpleMapBundle\Modules\Frontend\ModuleSimpleMapView',
        'simple_map_category_list' => 'Srhinow\SimpleMapBundle\Modules\Frontend\ModuleSimpleMapCategoryList',
    ],
]);

/*
 * -------------------------------------------------------------------------
 * Models
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_MODELS']['tl_simple_map'] = \Srhinow\SimpleMapBundle\Models\SimpleMapModel::class;
$GLOBALS['TL_MODELS']['tl_simple_map_pin'] = \Srhinow\SimpleMapBundle\Models\SimpleMapPinModel::class;
$GLOBALS['TL_MODELS']['tl_simple_map_category'] = \Srhinow\SimpleMapBundle\Models\SimpleMapCategoryModel::class;
