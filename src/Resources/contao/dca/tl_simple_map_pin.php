<?php

declare(strict_types=1);

use Contao\DC_Table;
use Contao\DataContainer;

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

/*
 * Table tl_simple_map_pin.
 */
$GLOBALS['TL_DCA']['tl_simple_map_pin'] = [
    // Config
    'config' => [
        'dataContainer' => DC_Table::class,
        'ptable' => 'tl_simple_map',
        'enableVersioning' => true,
        'onsubmit_callback' => [
            ['srhinow.simple_map_bundle.listeners.dca.simple_map_pin', 'onSubmitCallbacks'],
        ],
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'pid,published' => 'index',
            ],
        ],
    ],
    // List
    'list' => [
        'sorting' => [
            'mode' => DataContainer::MODE_SORTABLE,
            'fields' => ['title'],
            'flag' => DataContainer::SORT_INITIAL_LETTER_ASC,
            'panelLayout' => 'filter;sort,search,limit',
        ],
        'label' => [
            'fields' => ['title', 'street', 'nr', 'postal', 'city', 'category:tl_simple_map_category.title'],
            'showColumns' => true,
        ],
        'global_operations' => [
            'importFromCsv' => [
                'href' => 'key=importFromCsv',
                'class' => 'header_map_pin_import',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif',
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.svg',
            ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.svg',
                'attributes' => '
                    onclick="if (!confirm(\''.($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null).'\')) return false; 
                    Backend.getScrollOffset();"',
            ],
            'show' => [
                'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['show'],
                'href' => 'act=show',
                'icon' => 'show.gif',
            ],
        ],
    ],
    // Palettes
    'palettes' => [
        'default' => '
		    {main_legend},title,category;
		    {address_legend},street,nr,postal,city,language;
		    {map_legend},setNewGeo,mapLat,mapLon,popupText,popupOpen;
		    {general_legend},published
		    ',
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'foreignKey' => 'tl_page.title',
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'belongsTo', 'load' => 'lazy'],
        ],
        'tstamp' => [
            'sorting' => true,
            'flag' => 8,
            'eval' => ['rgxp' => 'datime'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'title' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['title'],
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'category' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['category'],
            'filter' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'select',
            'foreignKey' => 'tl_simple_map_category.title',
            'eval' => ['includeBlankOption' => true, 'chosen' => true, 'tl_class' => 'w50'],
            'sql' => "varchar(64) NOT NULL default ''",
            'relation' => ['type' => 'belongsTo', 'load' => 'lazy'],
        ],
        'street' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['street'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => [
                'maxlength' => 128,
                'feEditable' => true,
                'feViewable' => true,
                'feGroup' => 'address',
                'tl_class' => 'w50',
            ],
            'sql' => "varchar(128) NOT NULL default ''",
        ],
        'nr' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['nr'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => [
                'maxlength' => 28,
                'feEditable' => true,
                'feViewable' => true,
                'feGroup' => 'address',
                'tl_class' => 'w50',
            ],
            'sql' => "varchar(28) NOT NULL default ''",
        ],
        'postal' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['postal'],
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => [
                'maxlength' => 32,
                'feEditable' => true,
                'feViewable' => true,
                'feGroup' => 'address',
                'tl_class' => 'w50',
            ],
            'sql' => "varchar(32) NOT NULL default ''",
        ],
        'city' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['city'],
            'exclude' => true,
            'filter' => true,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => [
                'maxlength' => 255,
                'feEditable' => true,
                'feViewable' => true,
                'feGroup' => 'address',
                'tl_class' => 'w50',
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'language' => [
            'exclude' => true,
            'filter' => true,
            'inputType' => 'select',
            'default' => 'de',
            'eval' => [
                'includeBlankOption' => true,
                'chosen' => true,
                'rgxp' => 'locale',
                'feEditable' => true,
                'feViewable' => true,
                'feGroup' => 'personal',
                'tl_class' => 'w50',
            ],
            'options_callback' => static function () {
                return Contao\System::getContainer()->get('contao.intl.locales')->getLocales(null, true);
            },
            'sql' => "varchar(5) NOT NULL default 'de'",
        ],
        'setNewGeo' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['setnewgeo'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'sql' => "char(1) NOT NULL default ''",
            'eval' => ['tl_class' => 'clr'],
            'save_callback' => [
                ['srhinow.simple_map_bundle.listeners.dca.simple_map_pin', 'setNewGeoLatLon'],
            ],
        ],
        'mapLat' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['mapLat'],
            'inputType' => 'text',
            'eval' => ['tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'mapLon' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['mapLon'],
            'inputType' => 'text',
            'eval' => ['tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'popupText' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['popupText'],
            'exclude' => true,
            'search' => true,
            'filter' => false,
            'inputType' => 'textarea',
            'eval' => ['mandatory' => false, 'cols' => '10', 'rows' => '10', 'rte' => 'tinyMCE', 'tl_class' => 'clr'],
            'sql' => 'blob NULL',
        ],
        'popupOpen' => [
            'exclude' => true,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true, 'tl_class=' => 'clr'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'published' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 2,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true, 'tl_class=' => 'clr'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'importCsvFile' => [
            'exclude' => true,
            'inputType' => 'select',
            'options_callback' => ['srhinow.simple_map_bundle.listeners.dca.simple_map_pin', 'getCsvFilesAsOptions'],
            'eval' => [],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'importMatchMapPinFields' => [
            'exclude' => true,
            'inputType' => 'multiColumnWizard',
            'eval' => [
                'columnFields' => [
                    'tl_csv_fields' => [
                        'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['tl_csv_fields'],
                        'exclude' => true,
                        'inputType' => 'text',
                        'eval' => ['style' => 'width:250px', 'decodeEntities' => true],
                    ],
                    'tl_simple_map_pin' => [
                        'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['tl_simple_map_pin'],
                        'exclude' => true,
                        'inputType' => 'select',
                        'options_callback' => ['srhinow.simple_map_bundle.listeners.dca.simple_map_pin', 'getMapPinFieldOptions'],
                        'eval' => ['style' => 'width:250px', 'includeBlankOption' => true, 'chosen' => true],
                    ],
                    'custom_value' => [
                        'label' => &$GLOBALS['TL_LANG']['tl_simple_map_pin']['custom_value'],
                        'exclude' => true,
                        'inputType' => 'text',
                        'eval' => ['style' => 'width:100px', 'decodeEntities' => true],
                    ],
                ],
            ],
            'sql' => 'blob NULL',
        ],
        'importOffset' => [
            'inputType' => 'text',
            'exclude' => true,
            'default' => 0,
            'eval' => ['rgxp' => 'digit', 'doNotCopy' => true, 'tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'importLimit' => [
            'inputType' => 'text',
            'exclude' => true,
            'default' => 50,
            'eval' => ['rgxp' => 'digit', 'doNotCopy' => true, 'tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '50'",
        ],
    ],
];
