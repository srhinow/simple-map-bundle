<?php

declare(strict_types=1);

use Contao\DC_Table;
use Contao\DataContainer;

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

/*
 * Table tl_simple_map.
 */
$GLOBALS['TL_DCA']['tl_simple_map'] = [
    // Config
    'config' => [
        'dataContainer' => DC_Table::class,
        'ctable' => ['tl_simple_map_pin'],
        'switchToEdit' => true,
        'enableVersioning' => true,
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => DataContainer::MODE_SORTED,
            'fields' => ['title'],
            'flag' => DataContainer::SORT_INITIAL_LETTER_ASC,
        ],
        'label' => [
            'fields' => ['title', 'alias'],
            'format' => '%s <span style="color:#b3b3b3; padding-left:3px;">[%s]</span>',
        ],
        'global_operations' => [
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif',
            ],
            'map_pin' => [
                'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['map_pin'],
                'href' => 'table=tl_simple_map_pin',
                'icon' => $GLOBALS['BE_SIMPLE_MAP_BUNDLE']['PROPERTIES']['PUBLICSRC'].'/icons/location_pin.png',
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => '
                    onclick="if (!confirm(\''.($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null).'\')) return false; 
                    Backend.getScrollOffset();"',
            ],
            'show' => [
                'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['show'],
                'href' => 'act=show',
                'icon' => 'show.gif',
            ],
        ],
    ],
    // Palettes
    'palettes' => [
        '__selector__' => ['do_mapbox'],
        'default' => '
		    {main_legend},title,alias;
		    {address_legend},street,nr,postal,city,language,setNewPin;
		    {map_legend},setNewGeo,mapLat,mapLon,mapZoom,leafletCustomScript;
		    {mapbox_legend},do_mapbox
		    ',
    ],
    // Subpalettes
    'subpalettes' => [
        'do_mapbox' => 'mapboxApiKey,mapboxStyle',
    ],
    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'title' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['title'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50', 'unique' => true],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'alias' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['alias'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => [
                'rgxp' => 'alnum',
                'doNotCopy' => true,
                'spaceToUnderscore' => true,
                'maxlength' => 128,
                'tl_class' => 'w50',
            ],
            'sql' => "varchar(64) NOT NULL default ''",
            'save_callback' => [
                ['srhinow.simple_map_bundle.listeners.dca.simple_map', 'generateAlias'],
            ],
        ],
        'leafletCustomScript' => [
            'exclude' => true,
            'search' => true,
            'default' => 'mymap.scrollWheelZoom.disable();',
            'inputType' => 'textarea',
            'eval' => ['tl_class' => 'clr long', 'rte' => 'ace|js'],
            'sql' => "text NOT NULL default 'mymap.scrollWheelZoom.disable();'",
        ],
        'do_mapbox' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['do_mapbox'],
            'exclude' => true,
            'filter' => true,
            'inputType' => 'checkbox',
            'eval' => ['submitOnChange' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'mapboxApiKey' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'clr long', 'unique' => true],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'mapboxStyle' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'clr long', 'decodeEntities'=> true],
            'sql' => "varchar(255) NOT NULL default 'mapbox/streets-v12'",
        ],

        'street' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['street'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => [
                'maxlength' => 128,
                'feEditable' => true,
                'feViewable' => true,
                'feGroup' => 'address',
                'tl_class' => 'w50',
            ],
            'sql' => "varchar(128) NOT NULL default ''",
        ],
        'nr' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['nr'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => [
                'maxlength' => 28,
                'feEditable' => true,
                'feViewable' => true,
                'feGroup' => 'address',
                'tl_class' => 'w50',
            ],
            'sql' => "varchar(28) NOT NULL default ''",
        ],
        'postal' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['postal'],
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => [
                'maxlength' => 32,
                'feEditable' => true,
                'feViewable' => true,
                'feGroup' => 'address',
                'tl_class' => 'w50',
            ],
            'sql' => "varchar(32) NOT NULL default ''",
        ],
        'city' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['city'],
            'exclude' => true,
            'filter' => true,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => [
                'maxlength' => 255,
                'feEditable' => true,
                'feViewable' => true,
                'feGroup' => 'address',
                'tl_class' => 'w50',
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'language' => [
            'exclude' => true,
            'filter' => true,
            'inputType' => 'select',
            'default' => 'de',
            'eval' => [
                'includeBlankOption' => true,
                'chosen' => true,
                'rgxp' => 'locale',
                'feEditable' => true,
                'feViewable' => true,
                'feGroup' => 'personal',
                'tl_class' => 'w50',
            ],
            'options_callback' => static function () {
                return Contao\System::getContainer()->get('contao.intl.locales')->getLocales(null, true);
            },
            'sql' => "varchar(5) NOT NULL default 'de'",
        ],
        'setNewGeo' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['setNewGeo'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'sql' => "char(1) NOT NULL default ''",
            'eval' => ['tl_class' => 'clr'],
            'save_callback' => [
                ['srhinow.simple_map_bundle.listeners.dca.simple_map', 'setNewGeoLatLon'],
            ],
        ],
        'setNewPin' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['setNewPin'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'sql' => "char(1) NOT NULL default ''",
            'eval' => ['tl_class' => 'clr'],
            'save_callback' => [
                ['srhinow.simple_map_bundle.listeners.dca.simple_map', 'createPinFromMapSettings'],
            ],
        ],
        'mapLat' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['mapLat'],
            'inputType' => 'text',
            'eval' => ['tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'mapLon' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['mapLon'],
            'inputType' => 'text',
            'eval' => ['tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'mapZoom' => [
            'label' => &$GLOBALS['TL_LANG']['tl_simple_map']['mapZoom'],
            'inputType' => 'text',
            'eval' => ['maxlength' => 3, 'tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '5'",
        ],
    ],
];
