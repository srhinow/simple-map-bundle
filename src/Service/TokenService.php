<?php

declare(strict_types=1);


namespace Srhinow\SimpleMapBundle\Service;

use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

readonly class TokenService
{
    public function __construct(
        private CsrfTokenManagerInterface $csrfTokenManager,
        private string                    $csrfTokenName
    ){}

    public function generateToken(): string
    {
        return $this->csrfTokenManager->getToken($this->csrfTokenName)->getValue();
    }

    public function checkToken(string $tokenValue): bool
    {
        $token = new CsrfToken($this->csrfTokenName, $tokenValue);

        return $this->csrfTokenManager->isTokenValid($token);
    }
}