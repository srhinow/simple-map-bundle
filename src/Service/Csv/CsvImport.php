<?php
/**
 * Created by fachstellen.de.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 12.08.22
 */

namespace Srhinow\SimpleMapBundle\Service\Csv;

use Contao\Backend;
use Contao\BackendTemplate;
use Contao\Config;
use Contao\Controller;
use Contao\CoreBundle\Exception\AccessDeniedException;
use Contao\CoreBundle\Exception\ResponseException;
use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\Dbafs;
use Contao\DropZone;
use Contao\Environment;
use Contao\Files;
use Contao\FilesModel;
use Contao\FileUpload;
use Contao\Input;
use Contao\Message;
use Contao\SelectMenu;
use Contao\StringUtil;
use Contao\System;
use Contao\TextField;
use League\Csv\Reader;
use League\Csv\Statement;
use MenAtWork\MultiColumnWizardBundle\Contao\Widgets\MultiColumnWizard;
use Srhinow\SimpleMapBundle\Helper\MapPinHelper;
use Srhinow\SimpleMapBundle\Models\SimpleMapPinModel;
use Srhinow\SimpleMapBundle\Service\ModeService;
use Srhinow\SimpleMapBundle\Service\TokenService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class CsvImport extends Backend
{
    /**
     * @var array
     */
    protected $seperators = [];

    /**
     * @var string
     */
    protected $formId = 'map_pin_import';

    /**
     * @var string
     */
    protected $saveMapPinFile = 'files/match_map_pin_fields.txt';


    /**
     * @var string
     */
    protected $csvUploadFolder = 'files/csv_imports';

    /**
     * @var string
     */
    protected $strCaseTable = 'tl_spree_map_pin';

    /**
     * @var int
     */
    protected $importLimit = 0;

    /**
     * @var int
     */
    protected $importOffset = 0;


    protected $strRootDir;

    /**
     * Import the Files library
     */
    public function __construct(
        private readonly RequestStack $request,
        private readonly TokenService $tokenService
    )
    {
        parent::__construct();
        $this->seperators = $GLOBALS['BE_SIMPLE_MAP_BUNDLE']['csv_seperators'];
        $this->setStrRootDir();
        $this->mkCsvImportFolder();
    }

    /**
     * Import map pins
     */
    public function importMapPins()
    {
        if (Input::post('FORM_SUBMIT') == $this->formId) {
            $this->savePropertiesInCookie();
            $this->mkCsvImportFolder();

            if (Input::post('saveMatchFields') == 1) {
                $this->saveCsvMatch(Input::post('importMatchMapPinFields'),$this->saveMapPinFile);
            }

            if($csvFile = $this->getFileObject()) {
                $this->extractCsvFile($csvFile->path);
            }
        }

//        if(array_key_exists('importLoop', $_COOKIE) && isset($_COOKIE['importLoop'])) {
//            self::setCookie('importLoop','',time() - Config::get('sessionTimeout'));
//            $this->setPostValuesFromCookies();
//            $csvFile = $this->getFileObject();
//
//            if(false !== $csvFile) {
//                $this->extractCsvFile($csvFile->path);
//            }
//        }

        $objTemplate = new BackendTemplate('be_map_pin_import_csv');
        $objTemplate->backlink = StringUtil::ampersand(str_replace('&key=importFromCsv', '', $this->request->getCurrentRequest()->getRequestUri()));
        $objTemplate->message = Message::generate();
        $objTemplate->csv_seperators = $this->seperators;
        $objTemplate->csv_current_seperators = Input::post('separator');
        $objTemplate->current_dfr = Input::post('drop_first_row');

        $GLOBALS['TL_CSS'][] = 'assets/dropzone/css/dropzone.min.css';
        $GLOBALS['TL_JAVASCRIPT'][] = 'assets/dropzone/js/dropzone.min.js';

        $objTemplate->uploadAjaxUrl = html_entity_decode($this->getUploadAjaxUrl('ajax=csv_upload&path=' . urlencode($this->csvUploadFolder)));
        $objTemplate->strAccepted = implode(',', array_map(static function ($a) {
            return '.' . $a;
        }, StringUtil::trimsplit(',', strtolower(Config::get('uploadTypes')))));
        $objTemplate->intMaxSize = round(FileUpload::getMaxUploadSize() / 1024 / 1024);

        if (isset($_COOKIE['importCsvFile'])) {
            $GLOBALS['TL_DCA']['tl_simple_map_pin']['fields']['importCsvFile']['value'] = $_COOKIE['importCsvFile'];
        }

        $objTemplate->importCsvFile = new SelectMenu(SelectMenu::getAttributesFromDca($GLOBALS['TL_DCA']['tl_simple_map_pin']['fields']['importCsvFile'],'importCsvFile',null,'importCsvFile','tl_simple_map_pin'));
        $savedMapPinMatches = $this->getSavedMatches($this->saveMapPinFile);
        $objTemplate->compareCsvMapPinFields = new MultiColumnWizard(MultiColumnWizard::getAttributesFromDca($GLOBALS['TL_DCA']['tl_simple_map_pin']['fields']['importMatchMapPinFields'], 'importMatchMapPinFields', $savedMapPinMatches, 'importMatchMapPinFields', 'tl_simple_map_pin'));


        $objTemplate->request_token = $this->tokenService->generateToken();
        $objTemplate->max_file_size = Config::get('maxFileSize');
        $objTemplate->formId = $this->formId;

        // Return the form
        return $objTemplate->parse();
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function mkCsvImportFolder(): void
    {
        // falls nicht vorhanden den Speicherort-Ordner anlegen
        if (!file_exists($this->strRootDir.'/'.$this->csvUploadFolder)) {
            if (!mkdir($this->strRootDir.'/'.$this->csvUploadFolder)) {
                throw new \Exception(sprintf(
                        'Verzeichnis "%s" konnte nicht angelegt werden',
                        $this->strRootDir.'/'.$this->csvUploadFolder)
                );
            }
        }
    }

    protected function getFileObject(): bool| FilesModel
    {
        $csvFile = FilesModel::findByPath($this->csvUploadFolder .'/'.Input::post('importCsvFile'));

        // Check the file names
        if (!$csvFile === null || strlen($csvFile->path) < 1)
        {
            Message::addError('Wählen Sie eine CSV-Datei für den Import aus.');
            self::setCookie('importLoop', '',time() - Config::get('sessionTimeout'));
            return false;
        }

        // Skip anything but .csv files
        if ($csvFile->extension != 'csv')
        {
            Message::addError(sprintf($GLOBALS['TL_LANG']['ERR']['filetype'], $csvFile->extension));
            self::setCookie('importLoop', '',time() - Config::get('sessionTimeout'));
            return false;
        }

        return $csvFile;
    }

    /**
     * Extract the Entry files and write the data to the database
     * @param $Files
     */
    public function extractCsvFile($csvFile)
    {
        $matchMapPinFields = (Input::post('importMatchMapPinFields'))?:$this->getSavedMatches($this->saveMapPinFile);
        $runTest = (int) Input::post('runTest');
        $separator = Input::post('separator');

//        if(Input::post('importOffset')) $this->importOffset = (int) Input::post('importOffset');
//        if(Input::post('importLimit')) $this->importLimit = (int) Input::post('importLimit');
        $counter = 0;
        $time = time();
        $mapPinRows = [];

        if (!ini_get("auto_detect_line_endings")) {
            ini_set("auto_detect_line_endings", '1');
        }

        /**
         * import CSV-File
         */
        $projectRoot = System::getContainer()->getParameter('kernel.project_dir');
        $csv = Reader::createFromPath($projectRoot.'/'.$csvFile,'r');
        $csv->setDelimiter($this->seperators[$separator]);
        $csv->setHeaderOffset(0);
        $countAll = $csv->count();
        if($this->importLimit == 0) $this->importLimit = $countAll;
        $header = $csv->getHeader();

        $stmt = (new Statement())
            ->offset($this->importOffset)
            ->limit($this->importLimit)
        ;

        $records = $stmt->process($csv);

        foreach($records as $offset => $record)
        {
            //Case-Feldzuweisungen abarbeiten
            $pinRow = [];

            if (is_array($matchMapPinFields) && count($matchMapPinFields)>0) {
                foreach($matchMapPinFields as $match) {
                    //wenn es zwar eine Reihe gibt, das Tabellenfeld aber leer ist
                    if(strlen($match['tl_simple_map_pin']) < 1) {
                        continue;
                    }

                    $fieldName = ((int) $match['tl_csv_fields'] > 0)
                        ?$header[$match['tl_csv_fields']]
                        :$match['tl_csv_fields'];

                    if (strlen($match['custom_value']) > 0) {
                        $pinRow[$match['tl_simple_map_pin']] = $match['custom_value'];
                    } else {
//                        dd(strlen($match['tl_csv_fields']),$fieldName,$record, $record[$fieldName]);
                        if(strlen($match['tl_csv_fields']) < 1 || !$record[$fieldName]) {
                            continue;
                        }

                        switch ($match['tl_simple_map_pin']) {
                            case 'popupText':
                                $fieldValue = nl2br($record[$fieldName]);
                                break;
                            default:
                                $fieldValue = $record[$fieldName];
                        }

                        $pinRow[$match['tl_simple_map_pin']] = $fieldValue;
                    }
                }
            }

            if($runTest === 1) {
                //alle Reihen sammeln
                $pinRows[] = implode(' | ',$pinRow);
            } else {

                /**
                 * Map-Pin:
                 * Store the values in the database
                 */
                $objMapPin = new SimpleMapPinModel();
                if (is_array($pinRow) && count($pinRow) > 0) {
                    $objMapPin->setRow($pinRow);
                }
                if (Input::get('table') === 'tl_simple_map_pin') {
                    $objMapPin->pid = (int) Input::get('id');
                }
                $objMapPin->tstamp = $time;
                $objMapPin->dateAdded = $time;
                $objMapPin->published = 1;
                $objMapPin->importCsvFile = Input::post('importCsvFile');
                $objMapPin->save();

                if (1 === (int)Input::post('setNewGeo')) {
                    $MapPinHelper = MapPinHelper::getInstance();
                    $searchData = $MapPinHelper->getGeoParamsFromModel($objMapPin);
                    if(is_array($searchData) && count($searchData) > 1) {
                        $MapPinHelper->setGeoLatLon((int) $objMapPin->id, (array) $searchData);
                    }
                }
            }

            $counter++;
            $cookieCounter = (int) isset($_COOKIE['counter'])?$_COOKIE['counter']:0;
            $countImportAll = $cookieCounter + $counter;
            self::setCookie('counter',$countImportAll,time() + Config::get('sessionTimeout'));
        }

        // Loop-Logik steuern
//        $nextOffset = $this->importOffset + $this->importLimit;
//        self::setCookie('importOffset',$nextOffset,time() + Config::get('sessionTimeout'));

//        if($nextOffset > $countAll) {
//            self::setCookie('importLoop','',time() - Config::get('sessionTimeout'));
//            self::setCookie('importOffset',0,time() - Config::get('sessionTimeout'));
//        } elseif($nextOffset <= $countAll) {
//            self::setCookie('importLoop','1',time() + Config::get('sessionTimeout'));
////            Backend::reload();
//        }

        // abschliessende Ausgabe
        if($runTest === '1') {

            // Notify the user
            $_SESSION['TL_ERROR'] = '';
            setcookie('BE_PAGE_OFFSET', 0, 0, '/');
            $this->resetCookiesAfterFinish();

            Message::addConfirmation(sprintf(
                $GLOBALS['TL_LANG']['tl_simple_map_pin']['csv_import_test']
                ,$countImportAll
                ,$countAll
                ,$csvFile
            ));

            Backend::reload();
        } else {
            // Notify the user
            $_SESSION['TL_ERROR'] = '';
            setcookie('BE_PAGE_OFFSET', 0, 0, '/');
            $this->resetCookiesAfterFinish();

            Message::addConfirmation(sprintf(
                $GLOBALS['TL_LANG']['tl_simple_map_pin']['csv_imported']
                ,$countImportAll
                ,$countAll
                ,$csvFile
            ));

            // Redirect
            Backend::redirect(str_replace('&key=importFromCsv', '', Environment::get('request')));
        }
    }

    /**
     * save the Form-data in Cookies when submitted the first time
     */
    protected function savePropertiesInCookie(): void
    {
        //div. Einstellung als Cookie speichern
        self::setCookie('separator', Input::post('separator'),time() + Config::get('sessionTimeout'));
        self::setCookie('runTest', Input::post('runTest'),time() + Config::get('sessionTimeout'));
        self::setCookie('importCsvFile',Input::post('importCsvFile'),time() + Config::get('sessionTimeout'));
//        self::setCookie('importOffset',Input::post('importOffset'),time() + Config::get('sessionTimeout'));
//        self::setCookie('importLimit', Input::post('importLimit'),time() + Config::get('sessionTimeout'));
        self::setCookie('importLoop', '1',time() + Config::get('sessionTimeout'));
        self::setCookie('counter',0,time() + Config::get('sessionTimeout'));
    }

    protected function setPostValuesFromCookies(): void
    {
        // ,'importCsvFile','importOffset'
        $arrCookies = ['separator','runTest','importLimit','importLoop','counter'];

        foreach($arrCookies as $cname) {
            if($_COOKIE[$cname]) {
                Input::setPost($cname, $_COOKIE[$cname]);
            }
        }
    }

    protected function resetCookiesAfterFinish(): void
    {
//        self::setCookie('importOffset',0,time() + Config::get('sessionTimeout'));
//        self::setCookie('importLimit',50,time() + Config::get('sessionTimeout'));
        self::setCookie('counter',0,time() + Config::get('sessionTimeout'));
        self::setCookie('runTest', '',time() - Config::get('sessionTimeout'));
        self::setCookie('importLoop','',time() - Config::get('sessionTimeout'));
    }
    /**
     * @param $arrMatches
     */
    protected function saveCsvMatch($arrMatches, $saveFile) {
        $File = Files::getInstance();
        $fp = $File->fopen($saveFile,'w');
        $File->fputs($fp, json_encode($arrMatches));
        $File->fclose($fp);
    }

    /**
     * @return array|mixed
     */
    protected function getSavedMatches($saveFile) {
        $arrContent = [];

        if(!file_exists($this->strRootDir.'/'.$saveFile)) return $arrContent;

        $File = Files::getInstance();
        $fp = $File->fopen($saveFile,'r');
        $jsonContent = stream_get_contents($fp);
        $arrContent = json_decode($jsonContent, true);

        return (is_array($arrContent) && count($arrContent) > 0)? $arrContent : [];
    }

    protected function getFirstRow($fdata) {
        $arrFirstRow = [];

        if(!is_array($fdata) || count($fdata) < 1) return $arrFirstRow;

        foreach($fdata as $k => $str) {
            $arrFirstRow[$k] = str_replace("\xEF\xBB\xBF",'',$str);
        }
        return $arrFirstRow;
    }


    public function getUploadAjaxUrl($strRequest, $arrUnset=array())
    {
        $pairs = array();
        $request = System::getContainer()->get('request_stack')->getCurrentRequest();

        if ($request->server->has('QUERY_STRING')) {
            parse_str($request->server->get('QUERY_STRING'), $pairs);
        }

        // Remove the request token and referer ID
        unset($pairs['rt'],$pairs['ref']);

        foreach ($arrUnset as $key) {
            unset($pairs[$key]);
        }

        // Merge the request string to be added
        if ($strRequest) {
            parse_str(str_replace('&amp;', '&', $strRequest), $newPairs);
            $pairs = array_merge($pairs, $newPairs, ['rt' => $this->tokenService->generateToken()]);
        }

        $uri = '';

        if (!empty($pairs)) {
            $uri = '?' . http_build_query($pairs, '', '&amp;', PHP_QUERY_RFC3986);
        }

        return (string) $this->getRoute() . $uri;
    }

    private function getRoute(): ?string
    {
        if (null === $this->request) {
            return null;
        }

        $request = Request::createFromGlobals();
        return substr($request->getBasePath().$request->getPathInfo(), strlen($request->getBasePath().'/'));
    }
    /**
     * @throws \Exception
     */
    public static function moveAjaxUpload()
    {
        $strFolder = Input::get('path');
        $strRootDir = System::getContainer()->getParameter('kernel.project_dir');

        /** @var ModeService $ModeService */
        $ModeService = System::getContainer()->get('srhinow.sumple_map_bundle.service.mode_service');

        if (!file_exists( $strRootDir. '/' . $strFolder)) {
            throw new AccessDeniedException('Folder "' . $strFolder . '" is not a directory.');
        }

        if (!preg_match('/^' . preg_quote(Config::get('uploadPath'), '/') . '/i', $strFolder)) {
            throw new AccessDeniedException('Parent folder "' . $strFolder . '" is not within the files directory.');
        }

        /** @var FileUpload $objUploader */
        $objUploader = new DropZone();

        // Process the uploaded files
        if (Input::post('FORM_SUBMIT') == 'map_pin_import') {

            // Generate the DB entries
            if (Dbafs::shouldBeSynchronized($strFolder)) {
                // Upload the files
                $arrUploaded = $objUploader->uploadTo($strFolder);

                if (empty($arrUploaded) && !$objUploader->hasError()) {
                    if (Input::post('ajax') === 'upload') {
                        throw new ResponseException(new Response($GLOBALS['TL_LANG']['ERR']['emptyUpload'], 400));
                    }

                    Message::addError($GLOBALS['TL_LANG']['ERR']['emptyUpload']);
                    Controller::reload();
                }

                foreach ($arrUploaded as $strFile) {
                    Dbafs::addResource($strFile);
                }
            } else {
                // Not DB-assisted, so just upload the file
                $arrUploaded = $objUploader->uploadTo($strFolder);
            }

            // HOOK: post upload callback
            if (isset($GLOBALS['TL_HOOKS']['postUpload']) && \is_array($GLOBALS['TL_HOOKS']['postUpload'])) {
                foreach ($GLOBALS['TL_HOOKS']['postUpload'] as $callback) {
                    if (\is_array($callback)) {
                        System::importStatic($callback[0])->{$callback[1]}($arrUploaded);
                    } elseif (\is_callable($callback)) {
                        $callback($arrUploaded);
                    }
                }
            }

            // Update the hash of the target folder
            if (Dbafs::shouldBeSynchronized($strFolder)) {
                Dbafs::updateFolderHashes($strFolder);
            }

            // Redirect or reload
            if (!$objUploader->hasError()) {
                if (Input::get('ajax') === 'csv_upload') {
                    /** @var Session $objSession */
                    $objSession = System::getContainer()->get('request_stack')->getSession();

                    if ($objSession->isStarted()) {
                        // Get the info messages only
                        $arrMessages = $objSession->getFlashBag()->get('contao.' . $ModeService->getMode() . '.info');
                        Message::reset();

                        if (!empty($arrMessages)) {
                            throw new ResponseException(new Response('<p class="tl_info">' . implode('</p><p class="tl_info">', $arrMessages) . '</p>', 201));
                        }
                    }

                    throw new ResponseException(new Response('', 201));
                }
            } elseif (Input::post('ajax') === 'csv_upload') {
                throw new ResponseException(new Response(Message::generateUnwrapped($ModeService->getMode(), true), 500));
            }
        }
    }

    /**
     * @return string
     */
    public function getCsvUploadFolder(): string
    {
        return $this->csvUploadFolder;
    }

    /**
     * @param string $csvUploadFolder
     */
    public function setCsvUploadFolder(string $csvUploadFolder): void
    {
        $this->csvUploadFolder = $csvUploadFolder;
    }

    /**
     * @return mixed
     */
    public function getStrRootDir()
    {
        return $this->strRootDir;
    }

    /**
     * @param mixed $strRootDir
     */
    public function setStrRootDir(): void
    {
        $this->strRootDir = System::getContainer()->getParameter('kernel.project_dir');
    }
}
