<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\SimpleMapBundle\EventListener\Dca;

use Contao\DataContainer;
use Contao\StringUtil;
use Srhinow\SimpleMapBundle\Helper\MapPinHelper;
use Srhinow\SimpleMapBundle\Helper\OsmHelper;
use Srhinow\SimpleMapBundle\Models\SimpleMapModel;
use Srhinow\SimpleMapBundle\Models\SimpleMapPinModel;

class SimpleMap extends SimpleMapListener
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Autogenerate an article alias if it has not been set yet.
     *
     * @param mixed
     * @param object
     *
     * @throws \Exception
     *
     * @return string
     */
    public function generateAlias($varValue, $dc)
    {
        $autoAlias = false;

        // Generate alias if there is none
        if (!\strlen($varValue)) {
            $autoAlias = true;
            $varValue = StringUtil::standardize($dc->activeRecord->title);
        }

        $objAlias = $this->Database->prepare('SELECT id FROM tl_simple_map WHERE id=? OR alias=?')
            ->execute($dc->id, $varValue)
        ;

        // Check whether the page alias exists
        if ($objAlias->numRows > 1) {
            if (!$autoAlias) {
                throw new \Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }

            $varValue .= '-'.$dc->id;
        }

        return $varValue;
    }

    /**
     * @param $varValue
     * @param $dc
     *
     * @return string
     */
    public function setNewGeoLatLon($varValue, $dc)
    {
        if (1 === (int) $varValue) {
            MapPinHelper::getInstance()->setGeoLatLonFromDc($dc);
        }

        return '';
    }

    /**
     * @param $varValue
     *
     * @return string
     */
    public function createPinFromMapSettings($varValue, DataContainer $dc)
    {
        if (1 > (int) $varValue) {
            return '';
        }

        $objMapPin = new SimpleMapPinModel();
        $objMapPin->pid = $dc->id;
        $objMapPin->title = $dc->activeRecord->title;
        $objMapPin->street = $dc->activeRecord->street;
        $objMapPin->nr = $dc->activeRecord->nr;
        $objMapPin->postal = $dc->activeRecord->postal;
        $objMapPin->city = $dc->activeRecord->city;
        $objMapPin->language = $dc->activeRecord->language;
        $objMapPin->mapLat = $dc->activeRecord->mapLat;
        $objMapPin->mapLon = $dc->activeRecord->mapLon;
        $objMapPin->popupText = nl2br($this->getPopupTextFromDc($dc));
        $objMapPin->published = '1';
        $objMapPin->save();

        if (\strlen($dc->activeRecord->mapLat) < 1 || \strlen($dc->activeRecord->mapLon) < 1) {
            $searchData = MapPinHelper::getInstance()->getGeoParamsFromDc($dc);
            if(!is_array($searchData) || count($searchData) < 1) {
                return '';
            }
            
            $geo = new OsmHelper();
            $data = $geo->getGeoData($searchData);

            if (\is_array($data[0])) {
                $objMap = SimpleMapModel::findByPk($dc->id);
                $objMap->mapLat = (float) $data[0]['lat'];
                $objMap->mapLon = (float) $data[0]['lon'];
                $objMap->save();

                //die GeoDaten fuer den Pin speichern
                $objMapPin->mapLat = (float) $data[0]['lat'];
                $objMapPin->mapLon = (float) $data[0]['lon'];
                $objMapPin->save();
            }
        }

        return '';
    }
}
