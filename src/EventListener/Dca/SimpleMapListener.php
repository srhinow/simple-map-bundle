<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\SimpleMapBundle\EventListener\Dca;

use Contao\Backend;
use Contao\DataContainer;

class SimpleMapListener extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * stellt den Text für den Popup zusammen.
     *
     * @return string
     */
    public function getPopupTextFromDc(DataContainer $dc): string
    {
        $text = '';
        if ($dc->activeRecord->title) {
            $text .= $dc->activeRecord->title."\n";
        }
        if ($dc->activeRecord->street) {
            $text .= $dc->activeRecord->street.' '.$dc->activeRecord->nr."\n";
        }
        if ($dc->activeRecord->city) {
            $text .= $dc->activeRecord->postal.' '.$dc->activeRecord->city;
        }

        return $text;
    }
}
