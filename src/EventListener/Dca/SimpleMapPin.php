<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\SimpleMapBundle\EventListener\Dca;

use Contao\Database;
use Contao\DataContainer;
use Contao\File;
use Contao\Folder;
use Contao\System;
use PharIo\FileSystem\Directory;
use Srhinow\SimpleMapBundle\Helper\MapPinHelper;
use Srhinow\SimpleMapBundle\Helper\OsmHelper;
use Srhinow\SimpleMapBundle\Models\SimpleMapPinModel;
use Srhinow\SimpleMapBundle\Service\Csv\CsvImport;

class SimpleMapPin extends SimpleMapListener
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onSubmitCallbacks($dc): void
    {
        $this->setFirstGeoLatLon($dc);
    }

    /**
     * @param $dc
     */
    public function setFirstGeoLatLon($dc): void
    {
        if (0 === (int) $dc->activeRecord->mapLat || 0 === (int) $dc->activeRecord->mapLon) {
            MapPinHelper::getInstance()->setGeoLatLonFromDc($dc);
        }
    }

    /**
     * @param $varValue
     * @param $dc
     *
     * @return string
     */
    public function setNewGeoLatLon($varValue, $dc)
    {
        if (1 === (int) $varValue) {
            MapPinHelper::getInstance()->setGeoLatLonFromDc($dc);
        }

        return '';
    }

    /**
     * gibt alle tl_simple_map_pin-Felder als select-options zurück
     * @return array
     */
    public function getMapPinFieldOptions() {

        $options = [];

        $arrMapPinFields = Database::getInstance()->listFields('tl_simple_map_pin');
//        dump($objMapPinTable);
//        die();
        if(!is_array($arrMapPinFields) || count($arrMapPinFields) < 1) return $options;

        foreach($arrMapPinFields as $field) {
            if(in_array($field['name'],['PRIMARY'])) {
                continue;
            }
            
            $options[$field['name']] = $field['name'];
        }

        return $options;
    }

    public function getCsvFilesAsOptions()
    {
        /** @var CsvImport $CsvImport */
        $CsvImport = System::getContainer()->get('srhinow.simple_map_bundle.service.csv_import');
        $strRootDir = System::getContainer()->getParameter('kernel.project_dir');
        $CsvFolder = $CsvImport->getCsvUploadFolder();

        $arrFiles = Folder::scan($strRootDir.'/'.$CsvFolder);
        if(count($arrFiles) < 1) {
            return [];
        }

        $arrOptions = [];
        foreach($arrFiles as $fileName) {
            $arrOptions[$fileName] = $fileName;
        }

        return $arrOptions;
    }
}
