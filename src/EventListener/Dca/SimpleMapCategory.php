<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\SimpleMapBundle\EventListener\Dca;

use Contao\Backend;
use Contao\StringUtil;


class SimpleMapCategory extends Backend
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onLoadCallbacks($dc): void
    {
    }

    public function onSubmitCallbacks($dc): void
    {
    }

    /**
     * Autogenerate an article alias if it has not been set yet.
     *
     * @param $varValue
     * @param $dc
     * @return bool|string
     */
    public function generateAlias($varValue, $dc): bool | string
    {
        $autoAlias = false;

        // Generate alias if there is none
        if (!\strlen($varValue)) {
            $autoAlias = true;
            $varValue = StringUtil::standardize($dc->activeRecord->title);
        }

        $objAlias = $this->Database->prepare('SELECT id FROM tl_simple_map_category WHERE id=? OR alias=?')
            ->execute($dc->id, $varValue)
        ;

        // Check whether the page alias exists
        if ($objAlias->numRows > 1) {
            if (!$autoAlias) {
                throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }

            $varValue .= '-'.$dc->id;
        }

        return $varValue;
    }
}
