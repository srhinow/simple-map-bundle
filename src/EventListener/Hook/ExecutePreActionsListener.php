<?php
/**
 * Created by fachstellen.de.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 18.08.22
 */

namespace Srhinow\SimpleMapBundle\EventListener\Hook;

use Contao\Input;
use Srhinow\SimpleMapBundle\Service\Csv\CsvImport;

class ExecutePreActionsListener
{
    public function __invoke(string $action): void
    {
        if ('fileupload' === $action && 'map_pin_import' === Input::post('FORM_SUBMIT')) {
            CsvImport::moveAjaxUpload();
        }
    }
}